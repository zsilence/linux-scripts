#!/usr/bin/env bash


path=$1
[ "$path" = '' ] && path="."

t=$2
[ "$t" = '' ] && t=15

find $path -type f -atime +$t

