#!/usr/bin/env bash


pss=$1
[ "$pss" = '' ] && pss=5
let pss+=1
ps -A --format pid,comm,user,%cpu,%mem --sort -%cpu,-%mem | head -n $pss

