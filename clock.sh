#!/usr/bin/env bash


gen_random_number() {
    local readonly a=84589
    local readonly c=45989
    local readonly m=217728
    echo $(( ($a * $1 + $c) % $m ))

    return 0
}

print_text() {
    local readonly IFS=$'\n'

    local readonly arr=( $1 )
    
    local len=$2
    if [[ $len = -1 ]]
    then
        for i in ${arr[@]}
        do
            [[ ${#i} -gt $len ]] && local len=${#i}
        done
    fi

    local readonly shx=$(( $X % (`tput cols` - $len) ))
    local readonly shy=$(( $Y % (`tput lines` - ${#arr[@]}) ))

    local readonly spaces=`printf "%-${shx}s" " "`
    clear

    for (( i=0; i<$shy; i++ ))
    do
        echo $'\n'
    done

    for line in ${arr[@]}
    do
        echo "$spaces$line"
    done

    return 0
}

exit_handler() {
    tput cnorm
    clear
    
    exit 0
}

screensaver() {
    trap exit_handler SIGINT
    tput civis

    local X=$(( `date +%s` % 3167 ))
    local Y=$(( `date +%s` % 2887 ))

    local d=$3
    [[ $d ]] || local d=4

    for (( ;; ))
    do
        local X=`gen_random_number $X`
        local Y=`gen_random_number $Y`

        for (( iter=0; iter<$d; iter++ ))
        do
            print_text "`figlet \`$1\``" $2
            #print_text "`cowsay $1`" $2
            sleep 1
        done
    done

    return 0
}


screensaver "date +%X" 45 $*
#screensaver "hi" -1 $*
