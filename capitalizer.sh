#!/usr/bin/env bash


in=$1
[ "$in" = '' ] && in="in.txt"

out=$2
[ "$out" = '' ] && out="out.txt" 

cat $in | awk -f capitalize.awk -v rs="." | awk -f capitalize.awk -v rs="!" | awk -f capitalize.awk -v rs="?" > $out

