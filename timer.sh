#!/usr/bin/env bash


echo "process name: $1"
str=`{ time $*; } 2>&1`
echo "execution time: `echo "$str" | sed -n 2p | grep -o "[0-9].\+$"`"

