#!/usr/bin/env awk -v rs="." -f


BEGIN {
    RS = rs
    ORS = ""
    FS = ""
    OFS = ""
}

NR != 1 {
    print(rs)
}

{
    match($0, "^[ \n\r\t]*.")
    sub("^[ \n\r\t]*.", toupper(substr($0, RSTART, RLENGTH)), $0)
    print($0)
}

